import os
import csv
from flask import Flask, render_template, request, redirect, url_for, jsonify, send_from_directory

app = Flask(__name__)

UPLOAD_DIRECORY = "Data\\"
app.config['UPLOAD_DIRECORY'] = UPLOAD_DIRECORY


@app.route('/files')
def index():
    return render_template('index.html')


@app.route('/files', methods=['GET', 'POST'])
def upload_file():
    uploaded_file = request.files['file']
    path = uploaded_file.filename
    if path.endswith('.csv'):
        if uploaded_file.filename != '':
            uploaded_file.save(os.path.join(
                app.config['UPLOAD_DIRECORY'],  uploaded_file.filename))
            verified_file_name = email_verification(uploaded_file.filename)
        return redirect(url_for('download_file', path=verified_file_name))
    return jsonify("File not found"), 400


@ app.route('/download-file/<path:path>')
def download_file(path):
    return send_from_directory(app.config['UPLOAD_DIRECORY'], path, as_attachment=True)


def email_verification(file_name):
    file_path = os.path.join(app.config['UPLOAD_DIRECORY'], file_name)
    verified_email = list()
    with open(file_path, 'r') as csvFile:
        csv_reader = csv.reader(csvFile)
        line_count = 0
        fields = csv_reader.__next__()
        fields.append('Valid')
        email_index = fields.index('Email')
        verified_email.append(fields)
        for row in csv_reader:
            if row[email_index].__contains__('e'):
                row.append('valid')
            else:
                row.append('invalid')
            line_count += 1
            verified_email.append(row)
    with open('Data/members-1.csv', 'w', newline='') as csvFile:
        csv_writer = csv.writer(csvFile)
        csv_writer.writerows(verified_email)
    return "members-1.csv"


if __name__ == "__main__":
    app.run(debug=True, port=8080)
